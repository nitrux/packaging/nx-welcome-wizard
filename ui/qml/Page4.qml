import QtQuick 2.10
import QtQuick.Controls 2.3

Page {
    width: 2560
    height: 1680

    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "images/backgrounds/page4.png"
    }
}
